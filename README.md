# Avaliação TIVIT - API REST #

[TOC]

# Geral

## Sobre
Essa API foi feita usando o framework Django.

Para simplificar a instalação, foi usado o banco de dados SQLite e VirtualEnv. 

Para Logs e Tests foi mantido o padrão do Django.

Para facilitar o tratamento dos requests, criei um middleware que olha as requisições em formato json e já coloca os dados no HTTPRequest.

Foi removido todas as APPs e Middlewares do Django que não eram necessários para essa API.

Foi criado uma migrate (/avaliacao/api/migrations/0002_importar_feiras.py) que pega o arquivo /avaliacao/api/migrations/DEINFO_AB_FEIRASLIVRES_2014.csv e cadastra no banco de dados


O model foi criado baseado no arquivo de descrição do csv(/CSV/DEINFO_DICIONARIOS_AB_FEIRASLIVRES/DEINFO_AB_FEIRASLIVRES_2014_Vari%E2%80%A0veis.csv), com exceção do ID, que deixei Inteiro.

Os arquivos de Log estão sendo gerados em /avaliacao/logs/, só criei 2 níveis, INFO e ERROR.

## Instalação

É necessário o Python 3.4 (não testei em outra versão) e o virtualenv (pip install virtualenv)


```
#!shellscript

$ virtualenv .env
$ source .env/bin/activate
$ pip install -r requirements.txt
$ cd avaliacao/
$ ./manage.py migrate
$ ./manage.py test
```


## Rodando a aplicação

```
#!shellscript

$ ./manage.py runserver
```



## Manipulando as Feiras

O objeto de feira é


```
#!javascript

{
  "id": 999,
  "are_ap": "3550308005209",
  "bairro": "JD SAPOPEMBA",
  "cod_dist": "78",
  "cod_subpref": "29",
  "distrito": "SAPOPEMBA",
  "lat": "-23613050",
  "lng": "-46498465",
  "logradouro": "RUA JOAO LOPES DE LIMA",
  "nome_feira": "FEIRA TESTE 888",
  "numero": "300.000000",
  "referencia": "TV AV SAPOPEMBA ALTURA 13000",
  "regiao_5": "Leste",
  "regiao_8": "Leste 1",
  "registro": "3112-7",
  "set_cens": "355030876000052",
  "subprefe": "VILA PRUDENTE"
}
```


O retorno padrão é 
```
#!javascript

{"feira": <object>}
```
 
No caso de lista/busca 
```
#!javascript

{"feiras": [<object>,<object>,...]}
```
 
No caso do DELETE 
```
#!javascript

{"success": True}
```


Em caso de erro, foi previsto: 

404: Quando um objeto não é encontrado
```
#!javascript
{"error": {"code": 404,  "message": "Feira Livre Not Found"}}
```

400: Quando os campos obrigatórios não são enviados no POST
```
#!javascript
{"error": {"code": 400,  "message": "Os campos xxx, yyy, zzz são obrigatórios"}} 
```


### Lista de Feiras

```
#!shellscript

$ curl -X GET -H "Content-Type: application/json" -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/FeiraLivre"
```
retorno:
```
#!javascript
{"feiras": [<object>,<object>,...]}  
``` 

#### com filtro, as opções são: bairro, regiao_5, nome_feira e distrito.

```
#!shellscript

$ curl -X GET -H "Content-Type: application/json" -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/FeiraLivre?bairro=vila"
```
retorno:
```
#!javascript
{"feiras": [<object>,<object>,...]}  
```


#### combinando mais de um filtro

```
#!shellscript

$ curl -X GET -H "Content-Type: application/json" -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/FeiraLivre?regiao_5=leste&bairro=sapo"
```
retorno:
```
#!javascript
{"feiras": [<object>,<object>,...]}  
```

### Incluíndo uma Feira 

```
#!shellscript

$ curl -X POST -H "Content-Type: application/json" -H "Cache-Control: no-cache" -d '{"are_ap": "3550308005209","bairro": "JD SAPOPEMBA","cod_dist": "78","cod_subpref": "29","distrito": "SAPOPEMBA","lat": "-23613050","lng": "-46498465","logradouro": "RUA JOAO LOPES DE LIMA","nome_feira": "FEIRA TESTE 888","numero": "300.000000","referencia": "TV AV SAPOPEMBA ALTURA 13000","regiao_5": "Leste","regiao_8": "Leste 1","registro": "3112-7","set_cens": "355030876000052","subprefe": "VILA PRUDENTE"}' "http://127.0.0.1:8000/api/FeiraLivre/"
```
retorno:
```
#!javascript
{"feira": <object>} 
```

### Obtendo uma Feira

```
#!shellscript

$ curl -X GET -H "Content-Type: application/json" -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/FeiraLivre/800/"
```
retorno:
```
#!javascript
{"feira": <object>} 
```

### Alterando uma Feira

```
#!shellscript

$ curl -X PUT -H "Content-Type: application/json" -H "Cache-Control: no-cache" -d '{"distrito":"novo distrito", "numero": "15"}' "http://127.0.0.1:8000/api/FeiraLivre/800/"
```
retorno:
```
#!javascript
{"feira": <object>} 
```

### Excluíndo uma Feira

```
#!shellscript

$ curl -X DELETE -H "Content-Type: application/json" -H "Cache-Control: no-cache" "http://127.0.0.1:8000/api/FeiraLivre/800/"
```
retorno:
```
#!javascript
{"success": True}
```