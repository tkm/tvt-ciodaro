from django.conf import settings
from django.utils.deprecation import MiddlewareMixin
import json

class APIMiddleware(MiddlewareMixin):
  def process_request(self, request):
    if (request.META.get('CONTENT_TYPE') == 'application/json'):
      request.META['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'
      request.data = None
      body = str(request.body.decode('utf8'))
      request.data = json.loads(body) if body else {}

