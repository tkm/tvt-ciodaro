# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.db import migrations, models

import os
import csv


def importar_feiras(apps, schema_editor):
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    FeiraLivre = apps.get_model("api", "FeiraLivre")

    fields = ('id','lng','lat','set_cens','are_ap','cod_dist','distrito','cod_subpref','subprefe','regiao_5','regiao_8','nome_feira','registro','logradouro','numero','bairro','referencia')
    feiras = None
    
    with open(os.path.join(BASE_DIR, 'DEINFO_AB_FEIRASLIVRES_2014.csv')) as csvfile:
        spamreader = csv.DictReader(csvfile, fieldnames=fields)
        feiras = [FeiraLivre(**row) for row in csv.DictReader(csvfile, fieldnames=fields) if row['id'].isdigit()]
      
    if bool(feiras):
        FeiraLivre.objects.bulk_create(feiras)

class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(importar_feiras),
    ]
