from django.views.generic import View
from django.http import JsonResponse
from .models import FeiraLivre

import json
import logging


logger = logging.getLogger(__name__)

def http404():
  return JsonResponse({'error': {'code': 404,  'message': 'Feira Livre Not Found'}}, status=404)

class FeiraLivreAPI(View):

  def save(self, request, feira):
    data = request.data
    info = [];
    is_new = not bool(feira.id)

    if not is_new:
      info.append('Alteração da FeiraLivre ID: %d' % feira.id)

    for k, v in data.items():
      if not is_new:
        info.append('- o campo %s foi alterado de "%s" para "%s"' % (k, getattr(feira, k), v ))
      
      setattr(feira, k, v)
    
    feira.save()

    if is_new:
      logger.info('Incluido a Feira Livre ID: %d com o conteúdo: %s' % (feira.id, json.dumps(data)))
    else:
      logger.info('\n\t'.join(info))

  def post(self, request):
    feira = FeiraLivre()

    required = ('lng','lat','set_cens','are_ap','cod_dist','distrito','cod_subpref','subprefe','regiao_5','regiao_8','nome_feira','registro','logradouro','numero','bairro')
    missing = [ k for k in required if request.data.get(k) is None]

    if bool(missing):
      logger.error('não foi enviado todos os campos obrigatórios!')
      return JsonResponse({'error': {'code': 400,  'message': 'Os campos %s são obrigatórios' % ', '.join(missing)}}, status=400)

    self.save(request, feira)
    return self.get(request, feira.id)

  def put(self, request, id):
    try:
      feira = FeiraLivre.objects.get(id=id)
      self.save(request, feira)
      return self.get(request, feira.id)
    except FeiraLivre.DoesNotExist:
      return http404()

  def delete(self, request, id):
    try:
      feira = FeiraLivre.objects.get(id=id)
      logger.info('Feira id:%s e nome: %s foi deletada.' % (feira.id, feira.nome_feira))
      feira.delete()

      return JsonResponse({'success': True})
    except FeiraLivre.DoesNotExist:
      return http404()

  def get(self, request, id=None):
    if id is None:
      search_fields = ('distrito','regiao_5','nome_feira','bairro')
      filter_fields = {'%s__icontains' % k:v for k,v in request.GET.items() if k in search_fields}

      if bool(filter_fields):
        return JsonResponse({'feiras': list(FeiraLivre.objects.filter(**filter_fields).values())})
      else:
        return JsonResponse({'feiras': list(FeiraLivre.objects.all().values())})
    else:
      try:
        feira = FeiraLivre.objects.values().get(id=id)
        return JsonResponse({'feira': feira})
      except FeiraLivre.DoesNotExist:
        return http404()
