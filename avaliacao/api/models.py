from django.db import models

# Create your models here.

class FeiraLivre(models.Model):
  """
  Arquivo,Variável,Nome da variável,Descrição da variável,Fonte,Tipo,Tamanho
  DEINFO_AB_FEIRASLIVRES_2014,ID,Identificador,Número de identificação do estabelecimento georreferenciado por SMDU/Deinfo,SMDU/Deinfo,TEXTO,08
  DEINFO_AB_FEIRASLIVRES_2014,LONG,Longitude,"Longitude da localização do estabelecimento no território do Município, conforme MDC",SMDU/Deinfo,TEXTO,10
  DEINFO_AB_FEIRASLIVRES_2014,LAT,Latitude,"Latitude da localização do estabelecimento no território do Município, conforme MDC",SMDU/Deinfo,TEXTO,10
  DEINFO_AB_FEIRASLIVRES_2014,SETCENS,Setor censitário,Setor censitário conforme IBGE,IBGE,TEXTO,15
  DEINFO_AB_FEIRASLIVRES_2014,AREAP,Área de ponderação,Área de ponderação (agrupamento de setores censitários) conforme IBGE 2010,IBGE,TEXTO,13
  DEINFO_AB_FEIRASLIVRES_2014,CODDIST,Código do distrito conforme IBGE,Código do Distrito Municipal conforme IBGE,IBGE,TEXTO,09
  DEINFO_AB_FEIRASLIVRES_2014,DISTRITO,Distrito municipal,Nome do Distrito Municipal,SMDU/Deinfo,TEXTO,18
  DEINFO_AB_FEIRASLIVRES_2014,CODSUBPREF,Código da Subprefeitura,Código de cada uma das 31 Subprefeituras (2003 a 2012),SMDU/Deinfo,TEXTO,02
  DEINFO_AB_FEIRASLIVRES_2014,SUBPREF,Subprefeitura,Nome da Subprefeitura (31 de 2003 até 2012),SMDU/Deinfo,TEXTO,25
  DEINFO_AB_FEIRASLIVRES_2014,REGIAO5,Região conforme divisão do Município em 5 áreas,Região conforme divisão do Município em 5 áreas,SMDU/Deinfo,TEXTO,06
  DEINFO_AB_FEIRASLIVRES_2014,REGIAO8,Região conforme divisão do Município em 8 áreas,Região conforme divisão do Município em 8 áreas,SMDU/Deinfo,TEXTO,07
  DEINFO_AB_FEIRASLIVRES_2014,NOME_FEIRA,Nome da feira livre,Denominação da feira livre atribuída pela Supervisão de Abastecimento,SMSP/AB,TEXTO,30
  DEINFO_AB_FEIRASLIVRES_2014,REGISTRO,Registro da feira livre,Número do registro da feira livre na PMSP,SMSP/AB,TEXTO,6
  DEINFO_AB_FEIRASLIVRES_2014,LOGRADOURO,Logradouro,Nome do logradouro onde se localiza a feira livre,SMSP/AB,TEXTO,34
  DEINFO_AB_FEIRASLIVRES_2014,NUMERO,Número,Um número do logradouro onde se localiza a feira livre,SMSP/AB,TEXTO,5
  DEINFO_AB_FEIRASLIVRES_2014,BAIRRO,Bairro,Bairro de localização da feira livre,SMSP/AB,TEXTO,20
  DEINFO_AB_FEIRASLIVRES_2014,REFERENCIA,Ponto de referência,Ponto de referência da localização da feira livre,SMSP/AB,TEXTO,24
  """

  # id
  nome_feira = models.CharField(max_length=30)
  lng = models.CharField(max_length=10)
  lat = models.CharField(max_length=10)
  set_cens = models.CharField(max_length=15)
  are_ap = models.CharField(max_length=13)
  cod_dist = models.CharField(max_length=9)
  distrito = models.CharField(max_length=18)
  cod_subpref = models.CharField(max_length=2)
  subprefe = models.CharField(max_length=25)
  regiao_5 = models.CharField(max_length=6)
  regiao_8 = models.CharField(max_length=7)
  registro = models.CharField(max_length=6)
  logradouro = models.CharField(max_length=34)
  numero = models.CharField(max_length=5)
  bairro = models.CharField(max_length=20)
  referencia = models.CharField(max_length=24, null=True)

